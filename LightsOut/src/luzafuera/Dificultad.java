package luzafuera;

public class Dificultad {
	private int numeroNivel;
	private String nivel;

	public Dificultad(int n, String dif) {
		this.setNumeroNivel(n);
		this.setNivel(dif);
	}

	public int getNumeroNivel() {
		return numeroNivel;
	}

	public void setNumeroNivel(int numeroNivel) {
		this.numeroNivel = numeroNivel;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	@Override
	public String toString() {
		return nivel;
	}

}