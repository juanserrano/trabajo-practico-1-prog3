package luzafuera;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import java.awt.SystemColor;
import javax.swing.UIManager;

public class Game extends javax.swing.JFrame {

	private JTable table;
	private int moves;
	private JLabel lblMoves;
	private JButton btnReset;
	private int tamanioTabla;
	private String nombreJugador;
	private int bestScore;
	private PrimerVentana firstWindow;

	/**
	 * Create the application.
	 */
	public Game(int tamTabla, String nombrePlayer) {
		setResizable(false);
		this.nombreJugador = nombrePlayer;
		this.tamanioTabla = tamTabla;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		// ------------------INTERFAZ---------------------//
		this.setTitle("\u00A1 Lights Out !");
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("imagenes/logoungs.png"));
		this.setBounds(500, 200, 800, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		this.getContentPane().setBackground(Color.WHITE);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(175, 238, 238));
		panel.setBorder(UIManager.getBorder("FormattedTextField.border"));
		panel.setBounds(0, 497, 784, 64);
		getContentPane().add(panel);
		panel.setLayout(new GridLayout(0, 3, 0, 0));

		JLabel lblCantidadDeMovimientos = new JLabel("Cantidad de movimientos:");
		lblCantidadDeMovimientos.setBackground(Color.WHITE);
		lblCantidadDeMovimientos.setForeground(Color.BLUE);
		lblCantidadDeMovimientos.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblCantidadDeMovimientos);
		lblCantidadDeMovimientos.setFont(new Font("Tahoma", Font.PLAIN, 18));

		lblMoves = new JLabel("0");
		lblMoves.setForeground(Color.BLUE);
		panel.add(lblMoves);
		lblMoves.setFont(new Font("Tahoma", Font.PLAIN, 18));

		btnReset = new JButton("RESET");
		panel.add(btnReset);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(UIManager.getBorder("CheckBox.border"));
		panel_1.setBackground(new Color(175, 238, 238));
		panel_1.setBounds(2, 2, 780, 59);
		getContentPane().add(panel_1);
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));

		JLabel lblJugador = new JLabel("Jugador:");
		lblJugador.setForeground(Color.BLUE);
		lblJugador.setHorizontalAlignment(SwingConstants.CENTER);
		panel_1.add(lblJugador);
		lblJugador.setFont(new Font("Tahoma", Font.PLAIN, 22));

		JLabel lblNombreJugador = new JLabel("");
		lblNombreJugador.setForeground(Color.BLUE);
		panel_1.add(lblNombreJugador);
		lblNombreJugador.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblNombreJugador.setText(nombreJugador);

		table = new JTable(tamanioTabla, tamanioTabla);
		table.setBackground(Color.BLACK);
		table.setEnabled(false);
		table.setRowSelectionAllowed(false);

		table.setRowHeight(400 / tamanioTabla);
		table.setBounds(145, 72, 400, 400);
		this.getContentPane().add(table);

		JLabel lblMejorPuntaje = new JLabel("Mejor Puntaje:");
		lblMejorPuntaje.setVerticalAlignment(SwingConstants.TOP);
		lblMejorPuntaje.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
		lblMejorPuntaje.setBounds(592, 72, 166, 29);
		getContentPane().add(lblMejorPuntaje);

		JLabel lblBest = new JLabel("0");
		lblBest.setVerticalAlignment(SwingConstants.TOP);
		lblBest.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblBest.setHorizontalAlignment(SwingConstants.CENTER);
		lblBest.setBounds(602, 112, 104, 94);
		getContentPane().add(lblBest);

		setTable();
		randomTurnOff(tamanioTabla);
		// ------------------INTERFAZ---------------------//

		// ------------------CLICKEO---------------------//
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent x) {
				moves += 1;
				lblMoves.setText(moves + "");

				int row = table.rowAtPoint(x.getPoint());
				int column = table.columnAtPoint(x.getPoint());

				table.setValueAt(!((boolean) (table.getValueAt(row, column))), row, column);

				if (row + 1 < tamanioTabla) {
					table.setValueAt(!((boolean) (table.getValueAt(row + 1, column))), row + 1, column);
				}
				if (column + 1 < tamanioTabla) {
					table.setValueAt(!((boolean) (table.getValueAt(row, column + 1))), row, column + 1);
				}
				if (row - 1 >= 0) {
					table.setValueAt(!((boolean) (table.getValueAt(row - 1, column))), row - 1, column);
				}
				if (column - 1 >= 0) {
					table.setValueAt(!((boolean) (table.getValueAt(row, column - 1))), row, column - 1);
				}
				paint();
				if (funcionGanar(tamanioTabla) == true) {
					ganaste();
					lblBest.setText(bestScore + "");
				}
			}
		});

		btnReset.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				reset();
			}
		});

	}
	// ------------------CLICKEO---------------------//

	// ------------------COLOR_CELDAS---------------------//
	public class CustomTableCellRenderer extends DefaultTableCellRenderer {
		/**
			 * 
			 */
		private static final long serialVersionUID = 1L;

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {

			Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			boolean valor = (boolean) table.getValueAt(row, column);
			
			Color color = Color.black;
			if (valor == true) {
				color = Color.yellow;
			}
			cell.setBackground(color);
			cell.setForeground(color);
			return cell;
		}
	}
	// ------------------COLOR_CELDAS---------------------//

	// ------------------FUNCIONES---------------------//
	public void setTable() {
		moves = 0;
		for (int i = 0; i < tamanioTabla; i++) {
			for (int j = 0; j < tamanioTabla; j++) {
				table.setValueAt((Boolean) true, i, j);
			}
		}
	}

	public void paint() {
		for (int i = 0; i < tamanioTabla; i++) {
			table.getColumnModel().getColumn(i).setCellRenderer(new CustomTableCellRenderer());
		}
	}

	int randomWithRange(int max, int min) {
		int range = (max - min) + 1;
		return (int) (Math.random() * range) + min;
	}

	public void randomTurnOff(int size) {
		for (int i = randomWithRange(size + size % 2, 1); i > 0; i--) {
			int k = randomWithRange(size - 1, 0);
			int j = randomWithRange(size - 1, 0);
			table.setValueAt((Boolean) false, j, k);
			paint();
		}
	}

	public void reset() {
		moves = 0;
		lblMoves.setText(moves + "");
		table.setVisible(true);
		setTable();
		randomTurnOff(tamanioTabla);
	}

	public boolean funcionGanar(int size) {
		boolean gano = false;
		for (int f = 0; f < size; f++) {
			for (int c = 0; c < size; c++) {
				gano = gano || (boolean) table.getValueAt(f, c);
			}
		}
		return !gano;
	}

	public void ganaste() {
		table.setVisible(false);

		if (bestScore == 0) {
			bestScore = moves;
		} else if (moves < bestScore) {
			bestScore = moves;
		}

		Icon icono = new ImageIcon("imagenes/Winner.gif");
		int seleccion = JOptionPane.showOptionDialog(this,
				" �� GANASTE " + nombreJugador.toUpperCase() + " , FELICIDADES !! ", "� Lights Out !",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, icono,
				new Object[] { " Seguir jugando con la misma dificultad ", " Cambiar de dificultad ", " Salir " },
				"opcion 1");

		if (seleccion == 0)
			reset();

		if (seleccion == 1) {
			setVisible(false);
			firstWindow = new PrimerVentana();
			firstWindow.setVisible(true);
		}
		if (seleccion == 2)
			System.exit(0);
		
	}

}
