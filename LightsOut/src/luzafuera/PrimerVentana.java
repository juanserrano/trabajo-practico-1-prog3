package luzafuera;

import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.SystemColor;

public class PrimerVentana extends javax.swing.JFrame {
	private JTextField textoNombre;
	private Game segundaVentana;
	private JLabel labelNombre;
	private JLabel labelDificultad;
	private JComboBox comboBoxDificultad;
	private JButton botonJugar;
	private JButton botonSalir;

	/**
	 * Create the application.
	 */
	public PrimerVentana() {
		setBackground(SystemColor.activeCaption);
		getContentPane().setBackground(SystemColor.info);
		this.setResizable(false);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.setTitle(" \u00A1 Lights Out !");
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("imagenes/logoungs.png"));
		this.setBounds(500, 200, 500, 400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);

		labelNombre = new JLabel("Ingrese su nombre: ");
		labelNombre.setFont(new Font("Tahoma", Font.PLAIN, 15));
		labelNombre.setHorizontalAlignment(SwingConstants.CENTER);
		labelNombre.setBounds(43, 50, 137, 56);
		this.getContentPane().add(labelNombre);

		textoNombre = new JTextField();
		textoNombre.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textoNombre.setBounds(320, 70, 145, 20);
		this.getContentPane().add(textoNombre);
		textoNombre.setColumns(10);

		labelDificultad = new JLabel("Elija la dificultad del juego: ");
		labelDificultad.setFont(new Font("Tahoma", Font.PLAIN, 15));
		labelDificultad.setBounds(43, 216, 181, 20);
		this.getContentPane().add(labelDificultad);

		comboBoxDificultad = new JComboBox();
		comboBoxDificultad.addItem(new Dificultad(3, "F�cil"));
		comboBoxDificultad.addItem(new Dificultad(5, "Normal"));
		comboBoxDificultad.addItem(new Dificultad(9, "Dificil"));
		comboBoxDificultad.setBackground(Color.WHITE);

		comboBoxDificultad.setMaximumRowCount(3);
		comboBoxDificultad.setBounds(320, 218, 120, 20);
		this.getContentPane().add(comboBoxDificultad);

		botonJugar = new JButton("Jugar");
		botonJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Dificultad dificultadElegida = (Dificultad) comboBoxDificultad.getSelectedItem();
				segundaVentana = new Game(dificultadElegida.getNumeroNivel(), textoNombre.getText());
				setVisible(false);
				segundaVentana.setVisible(true);
			}
		});
		botonJugar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		botonJugar.setBounds(43, 322, 89, 23);
		this.getContentPane().add(botonJugar);

		botonSalir = new JButton("Salir");
		botonSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		botonSalir.setFont(new Font("Tahoma", Font.PLAIN, 15));
		botonSalir.setBounds(320, 322, 89, 23);
		this.getContentPane().add(botonSalir);
	}

}